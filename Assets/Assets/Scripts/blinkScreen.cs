﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blinkScreen : MonoBehaviour {

    public GameObject blinkedBg;
    public GameObject mainBg;
    public GameObject enemy;
    public GameObject buff;
    Rect gameBounds;
    public float worldScreenHeight;
    public float worldScreenWidth;
    public double timer = 2.0;
    public double time = 1.5;
    private bool blink = false;
    int pastScore = 0, antiHP = 1;

    // Use this for initialization
    void Start () {
        blinkedBg = GameObject.Find("bgBlink");
        enemy.SetActive(false);
        buff.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
        timer -= Time.deltaTime;
        int rand = Random.RandomRange(0, 3);
        if (timer <= 0 && blink == false)
        {
            timer = time;
            mainBg.SetActive(true);
            enemySpot();
            if (rand == 2)
                buffOpt();
            blinkedBg.SetActive(false);
            blink = true;
        }

        if (timer <= 0 && blink == true)
        {
            if (time <= 1.5)
                changeTime();
            else time = 1.5;
            if (globalScore.score == pastScore)
            {
                globalScore.health -= antiHP;
                antiHP *= 2;
            }
            else antiHP = 1;

            timer = time;
            if (buff.activeInHierarchy == true)
                buff.SetActive(false);
            mainBg.SetActive(false);
            enemy.SetActive(false);
            blinkedBg.SetActive(true);
            blink = false;
            pastScore = globalScore.score;
        }
	}
    
    //Information about world area and enemy spot
    void enemySpot()
    {        
        Vector3 pos = new Vector3 (worldScreenWidth + Random.Range (-7, 7), worldScreenHeight + Random.Range(-4, 4), 95);
        enemy.transform.position = pos;
        enemy.SetActive(true);
    }

    void getWorldSize()
    {
        worldScreenHeight = Camera.main.orthographicSize * 2f;
        worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
    }

    //Level difficult
    void changeTime()
    {
        if (globalScore.score > pastScore && time > 0.05)
            time -= 0.1;
        else if (globalScore.score == pastScore) 
            time += 0.2;
    }

    //Buff option
    void buffOpt()
    {
        Vector3 pos = new Vector3(worldScreenWidth + Random.Range(-7, 7), worldScreenHeight + Random.Range(-4, 4), 95);
        buff.transform.position = pos;
        buff.GetComponent<enemySpawn>().changePic();
        buff.SetActive(true);
    }
}
