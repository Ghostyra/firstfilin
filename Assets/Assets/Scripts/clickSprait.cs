﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clickSprait : MonoBehaviour {

    public GameObject enemy;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Collider2D coll = enemy.GetComponent<Collider2D>();

            if (coll.OverlapPoint(pos))
            {
                globalScore.score += 1;
                enemy.SetActive(false);
                enemy.GetComponent<enemySpawn>().changePic();
            }
        }
	}
}
