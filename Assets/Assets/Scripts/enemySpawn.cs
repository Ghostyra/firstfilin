﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemySpawn : MonoBehaviour {

    public Sprite[] spritePic;
    private int rand;   

    public void changePic()
    {
        rand = Random.Range(0, spritePic.Length);
        GetComponent<SpriteRenderer>().sprite = spritePic[rand];
    }

	// Use this for initialization
	public void Start () {
        changePic();
    }
	
	// Update is called once per frame
	void Update () {
	}
}
