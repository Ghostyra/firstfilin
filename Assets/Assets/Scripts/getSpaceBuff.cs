﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class getSpaceBuff : MonoBehaviour {

    public GameObject buff;
    public GameObject healthBonus;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            buff.SetActive(false);
            globalScore.health += 5;

            if (healthBonus.GetComponent<Text>().color != Color.cyan)
                healthBonus.GetComponent<Text>().color = Color.cyan;
            else healthBonus.GetComponent<Text>().color = Color.green;
        }
    }
}
